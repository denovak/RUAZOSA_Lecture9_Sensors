package hr.fer.tel.ruazosa.lecture9.sensorsdemo

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class SensorsDataActivity : AppCompatActivity(), SensorEventListener {


    private lateinit var sensorsData: TextView
    private lateinit var sensorManager: SensorManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sensors_data)
        sensorsData = findViewById(R.id.sensor_value)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }


    override fun onSensorChanged(event: SensorEvent) {
        var sensorValueAsString: String = ""
        for (i in event?.values) {
            sensorValueAsString += i.toString() + "\n"
        }
        sensorsData.text = sensorValueAsString
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {

    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    override fun onResume() {
        super.onResume()
        val sensor= sensorManager.getDefaultSensor(intent.extras.get("sensortype") as Int)
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }


}

