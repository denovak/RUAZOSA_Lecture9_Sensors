package hr.fer.tel.ruazosa.lecture9.sensorsdemo


import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView


class SensorsListActivity() : AppCompatActivity() {


    private lateinit var sensorListView: ListView
    private lateinit var sensorManager: SensorManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sensors_list)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val allSensors: List<Sensor> = sensorManager.getSensorList(Sensor.TYPE_ALL)

        sensorListView = findViewById(R.id.sensorslistview)
        sensorListView.adapter = SensorsAdapter(allSensors)

        sensorListView.setOnItemClickListener({adapterView, view, position, id ->
            val showSensorsDataActivity = Intent(this,  SensorsDataActivity::class.java)
            val sensor = adapterView.adapter.getItem(position) as Sensor
            showSensorsDataActivity.putExtra("sensortype", sensor.type)
            startActivity(showSensorsDataActivity)
        })

    }

    companion object {
        val TAG = "SensorsListActivity"
    }

    inner class SensorsAdapter(sensorsList: List<Sensor>) : BaseAdapter() {

        private var sensorsList = sensorsList


        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.sensor_in_list, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }

            vh.sensorName.text = sensorsList[position].name
            return view
        }

        override fun getItem(position: Int): Any {
            return sensorsList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return sensorsList.size
        }
    }

    private class ViewHolder(view: View?) {
        val sensorName: TextView

        init {
            this.sensorName = view?.findViewById<TextView>(R.id.sensor_name) as TextView
        }
    }


}
